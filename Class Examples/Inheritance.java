public class Inheritance {
    public static void main(String[] args){
        Car myCar = new Car("Ford", "Focus", 2003, "1.0 Litre Diesel", 4, 5);
        System.out.println("My Car is:\n\tMake: " + myCar.getMake() + "\n\tModel: " + myCar.getModel() + "\n\tEngine: " + myCar.getEngine() + "\n\tDoors: " + String.valueOf(myCar.getDoors()) + "\n\tWheels: " + String.valueOf(myCar.getWheels()) + "\n\tProduced In: " + String.valueOf(myCar.getYear()));
    }
}

class Vehicle{
    private String Make;
    private String Model;
    private int Year;
    Vehicle(String Make, String Model, int Year){
        this.Make = Make;
        this.Model = Model;
        this.Year = Year;
    }
    public String getMake(){
        return Make;
    }
    public String getModel(){
        return Model;
    }
    public int getYear(){
        return Year;
    }
}

class Car extends Vehicle{
    private String Engine;
    private int Wheels;
    private int Doors;
    Car(String Make, String Model, int Year, String Engine, int Wheels, int Doors){
        super(Make, Model, Year);
        this.Engine = Engine;
        this.Wheels = Wheels;
        this.Doors = Doors;
    }
    public String getEngine(){
        return Engine;
    }
    public int getWheels(){
        return Wheels;
    }
    public int getDoors(){
        return Doors;
    }
}