public class AbstractClasses {
    public static void main(String[] args){
        System.out.println("Abstract art sucks");

    }
}

interface Payable{
    public abstract float getPaymentAmount(); //No implementation. Also, float rather than double :D
}

abstract class Shape{
    private String colour;
    private int location, borderThickness;
    public Shape(String colour, int location, int borderThickness){
        this.colour = colour;
        this.location = location;
        this.borderThickness = borderThickness;
    }
    public String getColour(){
        return colour;
    }
    public int getLocation(){
        return location;
    }
    public int getBorderThickness(){
        return borderThickness;
    }
    public abstract void draw();
    public abstract double calcArea();
    public abstract void changeColour();
}

class Invoice implements Payable{
    private final String partNumber;
    private final String partDescription;
    private final int quantity;
    private final float pricePerItem;
    public Invoice(String partNumber, String partDescription, int quantity, float pricePerItem){
        this.partNumber = partNumber;
        this.partDescription = partDescription;
        this.quantity = quantity;
        this.pricePerItem = pricePerItem;
    }
    public int getQuantity(){
        return quantity;
    }
    public float getPricePerItem(){
        return pricePerItem;
    }
    public String getPartNumber(){
        return partNumber;
    }
    public String getPartDescription(){
        return partDescription;
    }
    @Override
    public float getPaymentAmount(){
        return getQuantity() * getPricePerItem();
    }
}