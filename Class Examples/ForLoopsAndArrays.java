public class ForLoopsAndArrays{
    public static void main(String[] args){
        int[] ages = {6, 23, 11, 16};
        String[] gradeLetters = {"A", "B", "C", "D", "E", "F", "U"};
        double[][] matrix = {{1.2, 2.4, 1.6, 3.4}, {2.4, 2.6, 2.1, 2.7}};
        System.out.println("Grades: ");
        for (int i = 0; i < gradeLetters.length; i++){
            System.out.print(String.valueOf(gradeLetters[i]) + " | ");
        }
        System.out.println("Ages: ");
        for (int i = 0; i < ages.length; i++){
            System.out.print(String.valueOf(ages[i]) + " | ");
        }
        System.out.println("Matrix?: ");
        for (int i = 0; i < matrix[0].length; i++){
            System.out.print(String.valueOf(" | " +matrix[0][i]) + " | ");
        }
    }
}