import javax.lang.model.util.SimpleTypeVisitor7;

public class Classes{
    public static void encapMod(){
        encapsulatingString = "Dumb joke";
    }
    static String encapsulatingString = "This is my favourite strong. <I mean string>";
    /* Since encapsulatingString is defined in this class, it can be accessed like a variable.
    However, other classes can access it by calling it with Classes.encapsulatingString (I.E: class.object)
    <As long as this class is public and not private>*/
    public static void main(String[] args){
        System.out.println("This is the first class called 'Classes'!");
        System.out.println("encapsulatingString = " + encapsulatingString);
        classA.main(args);   
        classB.main(args);
        classCar.main(args);
        classD conExamp = new classD("Bonjour!");
        encapMod();
        System.out.println("encapsulatingString = " + encapsulatingString);
        classA.extraMethodExample();
        System.out.println("encapsulatingString = " + encapsulatingString);
        classCar myCar = new classCar("Ford", "Ford GT40", 3); //Custom object creation.
        classCar.printCar(myCar);
        //^ printing custom object variables with custom method.
    }
}

class classA{
    public static void main(String[] args){
        System.out.println("This is the second class called 'ClassA'! And this is the main method.");
    }
    public static void extraMethodExample(){
        System.out.println("This is classA again, but this is the extraMethodExample method, not main.");
        Classes.encapsulatingString = "The device has been modified"; //Modify object in another class.
    }
}
class classB{
    public static void main(String[] args){
        System.out.println("This is the second class called 'ClassB'!");
    }
}
class classCar{
    private String CarMake; //Class Vars. They are private for security reasons. Making them not private would allow other classes to access them. This way, we need to write methods to access them and return them.
    private String CarModel;
    private int CarDoors;
    public static void main(String[] args){
        System.out.println("This is the third class called 'ClassC'! <Actually, it is ClassCar...>"); //Main kept because why not.
    }
    classCar(String CarMake, String CarModel, int CarDoors){ //Create new instance of this class. <This is an initilizer>
        this.CarMake = CarMake;
        this.CarModel = CarModel;
        this.CarDoors = CarDoors;
    }
    public static void printCar(classCar CarObj){
        System.out.println("MyCar: \nMake = " + CarObj.CarMake + "\nModel = " + CarObj.CarModel + "\nAmount of Doors = " + String.valueOf(CarObj.CarDoors));
    }
}

class classD{
    classD(String text){
        System.out.println("This is an example of a constructor!");
        System.out.println("The text arg is " + String.valueOf(text));
    }
}

class classE{
    public static void main(String[] args){
        System.out.println("This is the second class called 'ClassC'!");
    }
}