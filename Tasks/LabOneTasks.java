public class LabOneTasks{
    public static void main(String[] args){
        //I am using int because double is a wasteful use of memory.
        int a = 46;
        int b = 96;
        int c = a + b;
        System.out.println("The sum of " + String.valueOf(a) + " and " + String.valueOf(b) + " is " + String.valueOf(c));
    }
}