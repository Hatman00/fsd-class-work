package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    boolean fooled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void PrizeClaim(View view){
        TextView prizeText = findViewById(R.id.main_textview_lol);
        if (fooled){
            prizeText.setText(R.string.textview_default);
            fooled = false;
        }
        else{
            prizeText.setText(R.string.textview_fooled);
            fooled = true;
        }
    }
}