import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class LabSixTasks {
    public static void main(String[] args){
        /*try{
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/booksDB", "root", "Hatman00");
            Statement stmnt = con.createStatement();
            ResultSet rs=stmnt.executeQuery("SELECT * FROM table_books");
            while (rs.next()){
                System.out.println(rs.getInt(1) + " | " + rs.getString(2) + " | " + rs.getInt(3) + " | " + rs.getString(4));
            }
            con.close();
        }
        catch (Exception error){
            System.out.println(String.valueOf(error));
        }*/
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/gymstack", "root", "Hatman00");
            Statement stmnt = con.createStatement();
            ResultSet rs = stmnt.executeQuery("SELECT * FROM table_users");
            while (rs.next()) {
                System.out.println(rs.getInt(1) + " | " + rs.getString(2) + " | " + rs.getInt(3) + " | " + rs.getString(4));
            }
        } catch (Exception error) {
            System.out.println(String.valueOf(error));
        }
        System.out.println("Done");
    }
}
