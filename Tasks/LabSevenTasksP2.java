import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
public class LabSevenTasksP2{
    public static void main(String[] args){
      long start = System.currentTimeMillis();
      List<String> ls = new LinkedList<String>();
      String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
      for (String weekDay: weekDays)
         ls.add(weekDay);
      printDemo("ls:", ls);
      ls.set(ls.indexOf("Wed"), "Wednesday");
      printDemo("ls:", ls);
      ls.remove(ls.lastIndexOf("Fri"));
      printDemo("ls:", ls);
   } 
   static void printDemo(String title, List<String> ls)
   {
      System.out.print(title + " ");
      for (String s: ls)
         System.out.print(s + " ");
      System.out.println();
   }
}