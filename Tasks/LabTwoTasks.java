import java.util.Scanner;
public class LabTwoTasks {
    public static void main(String[] args){
        String d = "200";
        markCheck();
        StringTest();
        System.out.println(String.valueOf(toInt(d) + 40));
    }

    public static void markCheck(){
        int n;
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter grade: ");
        n = reader.nextInt();
        reader.close();
        if (n >= 95){
            System.out.println("Entered grade is an Exceptional 1st");
        }
        else if (n >= 85){
            System.out.println("Entered grade is an Outstanding 1st");
        }
        else if (n >= 75){
            System.out.println("Entered grade is a 1st");
        }
        else if (n >= 62){
            System.out.println("Entered grade is a 2.1");
        }
        else if (n >= 52){
            System.out.println("Entered grade is a 2.2");
        }
        else if (n >= 42){
            System.out.println("Entered grade is a 3");
        }
        else if (n >= 32){
            System.out.println("Entered grade is a Fail");
        }
        else{
            System.out.println("Entered grade is an Abject Fail");
        }
    }

    public static void StringTest(){
        String original = "java";
        StringBuilder result = new StringBuilder("Hi");
        int index = original.indexOf('a');
        System.out.println("Original Values:\n\toriginal: " + original + "\n\tresult: " + String.valueOf(result) + "\n\tindex: " + String.valueOf(index));
        result.setCharAt(0, original.charAt(0));
        System.out.println("New result:\n\t" + String.valueOf(result));
        result.setCharAt(1, original.charAt(original.length() - 1));
        System.out.println("New result:\n\t" + String.valueOf(result));
        result.insert(1, original.charAt(3)); //Origianlly had an index of 4 but 4 is out of range!
        System.out.println("New result:\n\t" + String.valueOf(result));
        result.append(original.substring(1, 4));
        System.out.println("New result:\n\t" + String.valueOf(result));
        result.insert(1, (original.substring(index, index + 2) + " "));
        System.out.println("New result:\n\t" + String.valueOf(result));
    }

    public static int toInt(String stArg){
        try{
            int result = Integer.parseInt(stArg);
            return result;
        }
        catch (Exception error){
            System.out.println("(toInt): Unable to convert " + stArg + " to int!");
            System.out.println(error);
            return 0;
        }
    }
}