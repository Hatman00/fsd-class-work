import java.util.Random;
public class LabSevenTasks {
    public static void main(String[] args){
        System.out.println("Hello... World :(");
        int[][] temp = new int[10][4];
        temp = gradeCreator();
        Grades studentgrade = new Grades(temp, "Computer Science");
        studentgrade.printGradeTable();
        System.out.println("Lowest Grade: " + String.valueOf(studentgrade.getMinimum()));
        System.out.println("Highest Grade: " + String.valueOf(studentgrade.getMaximum()));
        System.out.println(String.valueOf("Average Grade (For Student 4): " + studentgrade.getStudentAverage(4)));
    }

    public static int[][] gradeCreator(){
        int row = 0;
        int column = 0;
        int[][] classArray = new int[10][5];
        Random rand = new Random();
        while (column < 10){
            System.out.println(String.valueOf(row) + " | " + String.valueOf(column));
            if (row != 4){
                classArray[column][row] = rand.nextInt(101);
            }
            else{
                classArray[column][row] = classArray[column][row - 4] + classArray[column][row - 3] + classArray[column][row - 2] + classArray[column][row - 1];
                classArray[column][row] = Math.round(classArray[column][row] / 4);
            }
            row++;
            if (row > 4){
                row = 0;
                column++;
            }
        }
        return classArray; 
    }
}

class Grades{
    private int[][] studentGrades;
    private String courseName;
    Grades(int[][] studentGrades, String courseName){
        this.studentGrades = studentGrades;
        this.courseName = courseName;
    }
    public int[][] getStudentGrade(){
        return studentGrades;
    }
    public String getCourseName(){
        return courseName;
    }
    public void printGradeTable(){
        int column = 0;
        while (column < 10){
            System.out.println("-------------------------------------------------------------------");
            System.out.println("Student " + String.valueOf(column + 1) + " | " + String.valueOf(studentGrades[column][0]) + " | " + String.valueOf(studentGrades[column][1]) + " | " + String.valueOf(studentGrades[column][2]) + " | " + String.valueOf(studentGrades[column][3]) + " | Average: " + String.valueOf(studentGrades[column][4]));
            column++;
        }
        System.out.println("-------------------------------------------------------------------");
    }
    public int getMinimum(){
        int column = 0;
        int row = 0;
        int lowest = 100;
        while (column < 10){
            if (studentGrades[column][row] < lowest){
                lowest = studentGrades[column][row];
            }
            row++;
            if (row > 3){
                row = 0;
                column++;
            }
        }
        return lowest;
    }
    public int getMaximum(){
        int column = 0;
        int row = 0;
        int largest = 0;
        while (column < 10){
            if (studentGrades[column][row] > largest){
                largest = studentGrades[column][row];
            }
            row++;
            if (row > 3){
                row = 0;
                column++;
            }
        }
        return largest;
    }
    public int getStudentAverage(int student){
        return studentGrades[student][4];
    }
}